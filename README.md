![Creative Commons BY-SA 4.0 License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)


A 512KB RAM Card for the HP48GX Calculator
==========================================

This repository contains the KiCAD design files for an HP48GX calculator
RAM card.  The card contains a single 512KB SRAM chip and can be used in
either slot 1 or slot 2 of the calculator.

When the card is used in slot 2 it makes available 4 x 128KB banks of
storage that appear in the calculator as ports 2, 3, 4 and 5.

When used in slot 1, only one of the 128KB banks is available, which
will appear in the calculator as port 1.  Two switches, mounted on the
end of the board and accessible when the calculator card cover is removed,
allow control over which bank is used.

Prototypes
----------
The card is currently at revision 0 (initial design).  It has been
prototyped and found to be fully functional, but has a few issues
that could be addressed, if anyone wishes to do so, before further
boards are fabricated (see below).

A PDF of the schematic is available in [doc/schematic.pdf](doc/schematic.pdf).

Some photographs of my prototypes are also available:-

* Bare boards [doc/bare-board.jpg](doc/bare-board.jpg)
* A populated board [doc/populated-board.jpg](doc/populated-board.jpg)
* Two boards fitted into my HP48GX [doc/boards-in-situ.jpg](doc/boards-in-situ.jpg)

I manufactured the prototypes at [pcbway.com](http://pcbway.com), selecting
a yellow solder-resist, black silkscreen, immersion gold finish and gold
fingers with a bevelled edge.  The gold finish is necessary to avoid
corrosion on the battery pad and the connector fingers.

However, I also made a mistake: I ordered a 1.2 mm board thickness because
I had read somewhere that this was used by the original HP48 RAM cards.
Unfortunately 1.2 mm IS TOO THIN -- the cards do not seat properly in the
connector.  I had to pad the back of the cards with several layers of
sellotape (transparent sticky tape) to get a firm fitting.  I believe that
a standard 1.6 mm board would be the correct thickness to use (with a
bevelled edge for easy insertion).

Hand soldering the components onto the board is not too difficult (with a
fine-tipped soldering iron and a magnifier) -- I selected relatively
large footprint devices specifically to make assembly easier.  The parts
list is included below.

The current consumed by the CR2016 backup battery when the board is
outside of the calculator is less than 3 microamps, giving an expected
shelf-life for RAM contents of over 3 years for a 90mAh capacity battery.
I have yet to measure the current when the card is inside the calculator
and the calculator off (need to solder some wires to do this), but
hopefully it will not be much different.

Operation
---------
For use in slot 2, set the SW2 and SW3 switches (marked A18 and A17 on
the silkscreen) to their central positions (marked A for Automatic), and
set the SW1 switch (marked PROT on the silkscreen) to W (for Writeable).
After inserting the card into the calculator, issue the PINIT command
and you should see ports 2 through 5 become available in the ports list.

For use in slot 1, set the A17/18 switches to fixed 0 or 1 positions.
You may keep four different port 1 configurations in this way.  Just
be sure to switch off the calculator before moving the switches.

I have tested the functionality of the boards with the CHKMEM utility
available from [http://www.hpcalc.org/details/7394](http://www.hpcalc.org/details/7394),
and also with a few weeks of general usage.  Initially, I had some
strange issues with one of the boards that I eventually tracked down to
some dodgy soldering.  Other than that, I have not found any major
problems so far.

Note that I have not (yet) tested the card in an HP48SX calculator, but
I am hopeful that some combination of switch positions will allow it to
work in this calculator too.

When it comes time to change the backup battery you will notice that
the board must be removed from the calculator, and so there will be no
battery power available to power the SRAM chip during the battery
change process.  This is the reason for capacitors C1 & C4, they should
be able to supply enough current to support the SRAM during this
procedure.  Do not take too long though.  My own testing assumes 10
seconds is long enough to change a battery -- have the replacement
battery to hand and, of course, make a backup first!

Revision 0 Issues
-----------------
I have identified the following issues with revision 0 of the design:-

1. The switches could be a little closer to the edge of the board,
   allowing them to be more easily operated when in-situ in the
   calculator.
2. The C2 and C3 silkscreen legends are swapped.
3. The C4 silkscreen legend is lacking a '+' sign.
4. Removal of a card from a calculator can be awkward.  Consider the
   addition of a small hole to allow hooking the card out with a paper
   clip.

Parts List
----------
| Part                    | Description                  | Digikey Part Number |
|-------------------------|------------------------------|---------------------|
| R1                      | RES SMD 1M OHM 5% 1/8W 0805  | 311-1.0MARCT-ND     |
| R2, R3, R7, R8, R9, R12 | RES SMD 47K OHM 5% 1/8W 0805 | 311-47KARCT-ND      |
| R5, R6                  | RES SMD 10K OHM 5% 1/8W 0805 | 311-10KARCT-ND      |
| R4                      | RES SMD 4K7 OHM 5% 1/8W 0805 | 311-4.7KARCT-ND     |
| R15 - R22               | RES SMD 330 OHM 5% 1/8W 0805 | 311-330ARCT-ND      |
| C1, C4                  | CAP TANT 33UF 6.3V 10% 1206  | 478-8527-1-ND       |
| C2, C3                  | CAP CER 0.22UF 10V X7R 0805  | 445-8190-1-ND       |
| D1                      | DIODE ARRAY SCHOTTKY SOT23-3 | BAT54CFSCT-ND       |
| Q1                      | TRANS NPN 45V 0.1A SOT23     | BC847C-TPCT-ND      |
| U1                      | IC SRAM 4MBIT 45NS 32TSOP    | CY62148GN-45ZSXI-ND |
| SW1                     | SWITCH SLIDE SPDT 300MA 6V   | 401-2016-1-ND       |
| SW2, SW3                | SWITCH SLIDE SP3T 300MA 6V   | 401-2017-1-ND       |
| BT1                     | CR2016 3V 90mAh COIN CELL    | N/A                 |
| Battery Holder          | RETAINER 20MM COIN CELL SMD  | 36-3026-ND          |

Licence
-------
The materials in this repository are released under a Creative Commons
Attribution-ShareAlike 4.0 International License.  This means you can do
what you like with this material as long as you acknowledge my
contribution and distribute any changes you make under the same licence.
See [cc-by-sa](https://creativecommons.org/licenses/by-sa/4.0/) for details.

Paul Onions <paul.onions@acm.org>

22 June 2017
